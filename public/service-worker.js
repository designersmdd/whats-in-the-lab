/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

// Names of the two caches used in this version of the service worker.
// Change to v2, etc. when you update any of the local resources, which will
// in turn trigger the install event again.
const version = 'v1'
const DYNAMIC_CACHE = `dynamic-cache-${version}`
const PRE_CACHE = `pre-cache-${version}`
const ERROR_CACHE = `error-cache-${version}`

// A list of local resources we always want to be cached.
const PRE_CACHE_URLS = []

// The install handler takes care of precaching the resources we always need.
self.addEventListener('install', (event) => {
  event.waitUntil(
    caches
      .open(PRE_CACHE)
      .then((cache) => cache.addAll(PRE_CACHE_URLS))
      .then(self.skipWaiting())
  )
})

// The activate handler takes care of cleaning up old caches.
self.addEventListener('activate', (event) => {
  const currentCaches = [PRE_CACHE, DYNAMIC_CACHE, ERROR_CACHE]
  event.waitUntil(
    caches
      .keys()
      .then((cacheNames) => {
        return cacheNames.filter(
          (cacheName) => !currentCaches.includes(cacheName)
        )
      })
      .then((cachesToDelete) => {
        return Promise.all(
          cachesToDelete.map((cacheToDelete) => {
            return caches.delete(cacheToDelete)
          })
        )
      })
      .then(() => self.clients.claim())
  )
})

// Cache first
self.addEventListener('fetch', async (event) => {
  const cachedResponse = await caches.match(event.request)

  if (cachedResponse) return cachedResponse

  const response = await fetch(event.request)

  if (!response || response.status !== 200 || response.type !== 'basic')
    return response

  const responseToCache = response.clone()
  const cache = await caches.open(DYNAMIC_CACHE)
  await cache.put(event.request, responseToCache)

  return response
})
