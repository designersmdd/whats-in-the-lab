const gotoPage = (page, params) => {
  const isLocal =
    window.location.hostname === '127.0.0.1' ||
    window.location.hostname === 'localhost'

  const prefix = isLocal ? '/' : '/whats-in-the-lab/'
  const suffix = params ? `?${params}` : ''

  window.location = `${window.location.origin}${prefix}${page}.html${suffix}`
}
