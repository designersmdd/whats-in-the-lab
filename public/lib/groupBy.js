const groupBy = (list, keyGetter) => {
  const map = new Map()
  list.forEach((item) => {
    const key = keyGetter(item)
    const collection = map.get(key)

    collection ? collection.push(item) : map.set(key, [item])
  })
  return map
}
