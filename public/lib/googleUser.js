// https://developers.google.com/identity/sign-in/web/listeners
var auth2
var googleUser
var signInButton

const startGoogleAuth = () => gapi.load('auth2', initSigninV2)

const initSigninV2 = () => {
  auth2 = gapi.auth2.init({
    client_id:
      '491883684085-72r48mpu1970fv9tvojna1kbdmfjtk9r.apps.googleusercontent.com',
    scope: 'profile',
  })

  auth2.isSignedIn.listen(signInChanged)
  auth2.currentUser.listen(userChanged)

  // Sign in the user if they are currently signed in.
  if (auth2.isSignedIn.get() === true) auth2.signIn()

  refreshValues()

  signInButton = document.getElementById('googleSignInButton')
  attachSignIn(signInButton)
}

const attachSignIn = (element) => {
  auth2.attachClickHandler(element, {}, refreshValues, function (error) {
    alert(JSON.stringify(error, undefined, 2))
  })
}

const signInChanged = (isSignedIn) => {
  if (isSignedIn && googleUser) signInButton.style.display = 'none'
}

const userChanged = () => refreshValues

let ssoTries = 5
const refreshValues = () => {
  if (auth2) {
    var user = auth2.currentUser.get()
    var profile = user.getBasicProfile()
    googleUser = profile?.getEmail()
  }

  if (!googleUser && ssoTries > 0) {
    ssoTries--
    // Poor mans retry
    setTimeout(refreshValues, 1000)
  }
}

const getUser = () => googleUser
