# MDD stock control

A quick overview of the items (in stock) for the MDD lab.

MDD stock control is build with basic HTML / CSS / JS in order for everybody to be able to contribute.

This application is nothing more than a glorified google sheet viewer. In order to build this glorified layer we use the following technologies:

- [vue.js](https://vuejs.org/) for adding easier interactivity to pages
- [Stein](https://steinhq.com/) to interact with google sheets
- [tail wind css](https://tailwindcss.com/) as base styling
- [font awesome](https://fontawesome.com/) for iconography

# Authentication

🚨 You are able to use MDD stock control without authenticating.

However, some functionality requires us to have some form of user identification. [Google](https://developers.google.com/identity/sign-in/web/sign-in) is used in order to identify users where needed. [See how we use your data](https://gitlab.com/designersmdd/whats-in-the-lab/-/blob/main/public/lib/googleUser.js)

# Contribute

The main purpose of this repository is to continue evolving the MDD lab, making it faster and easier to use. Development of MDD stock control happens in the open on GitLab, and we are grateful to the community for contributing bugfixes and improvements. Read below to learn how you can take part in improving MDD stock control.

#### 1. Get the code on your computer

- [GitHub Desktop 🖥️](https://learn.hibbittsdesign.org/gitlab-githubdesktop/cloning-a-gitlab-repo)
- [Git kraken 🐙](https://support.gitkraken.com/working-with-repositories/open-clone-init/)
- [Terminal 🤖](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#:~:text=Cloning%20an%20Existing%20Repository,%22%20and%20not%20%22checkout%22.)

#### 2. Start project

1. [Install node](https://nodejs.org/en/download/)
2. [Start project](https://www.npmjs.com/package/http-server) using http-server
3. [View project locally](http://localhost:8080)

_Make sure you are viewing the project at [http://localhost:8080](http://localhost:8080) locally as this is the configured allowed endpoint for google._

#### 3. Happy coding

#### 4. Make a pull request

- [GitHub Desktop 🖥️](https://docs.github.com/en/free-pro-team@latest/desktop/contributing-and-collaborating-using-github-desktop/viewing-a-pull-request-in-github-desktop#:~:text=Under%20your%20repository%20name%2C%20click,the%20Open%20in%20Desktop%20button.)
- [Git kraken 🐙](https://support.gitkraken.com/working-with-repositories/pull-requests/#:~:text=If%20connected%20to%20a%20remote,selecting%20Start%20a%20pull%20request.)
- [Terminal 🤖](https://hackernoon.com/how-to-git-pr-from-the-command-line-a5b204a57ab1)

## Licence

MDD stock control has a [MIT licence](https://gitlab.com/designersmdd/whats-in-the-lab/-/blob/main/LICENCE)
